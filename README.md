I solemnly swear to fill this in once my deploy is successful and it's not the middle of the night

While I will get to that soon, I still want to register my gratitude to github user dlukes for this guide that saved my life and helped my flask service to coexist with other flask services on my server https://dlukes.github.io/flask-wsgi-url-prefix.html 