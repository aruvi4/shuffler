import random, csv, json

from time_util import write_time, needs_random

MENTORS = ['deepika.l@talentsprint.com', 
           'farmanul.h@talentsprint.com', 
           'ganapathi.b@talentsprint.com',
           'gayathri.e@talentsprint.com',
           'aruvi@talentsprint.com' 
           ]
MENTOR_CAP = 35

def read_students() -> list[str]:
    with open('students.csv', newline='') as csvfile:
        student_reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        return [row[1] for row in student_reader]
    
def reallocate() -> None:
    emails = read_students()
    random.shuffle(emails)
    evaluatees = dict()
    for evaluator in MENTORS:
        evaluatees[evaluator] = emails[:MENTOR_CAP]
        emails = emails[MENTOR_CAP:]
    first_mentor_copy = evaluatees[MENTORS[0]][:]
    print(len(first_mentor_copy))
    for evaluator, redundancy in zip(MENTORS, MENTORS[1:]):
        evaluatees[evaluator] += evaluatees[redundancy]
    evaluatees[MENTORS[-1]] += first_mentor_copy
    with open('allotted_evaluatees.json', 'w') as f:
        json.dump(evaluatees, f)

def get_evaluatees(evaluator_email: str) -> list[str]:
    if needs_random('evaluations_timestamp'):
        reallocate()
        write_time('evaluations_timestamp')
    with open('allotted_evaluatees.json', 'r') as f:
        evaluatees = json.load(f)
    return evaluatees.get(evaluator_email, "hello pls enter your email right, or if you're sure it's right, contact Aruvi")

