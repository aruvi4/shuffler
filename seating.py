#MY CODE LIKES IT WHEN NUM_STUDENTS < TOTAL CAPACITY ;)

import random, csv, json
from time_util import write_time, needs_random

extended_capacities = [25] * 7
rows = 'ABCDEFG'

ONE_DAY = 24 * 60 * 60

def read_students() -> list[str]:
    with open('students.csv', newline='') as csvfile:
        student_reader = csv.reader(csvfile, delimiter=",", quotechar='"')
        return [row[1] for row in student_reader]

def reallocate() -> None:
    emails = read_students()
    random.shuffle(emails)
    seats = dict()
    for row, capacity in zip(rows, extended_capacities):
        for col in range(capacity):
            if emails:
                student = emails.pop()
                seats[student] = f'{row}{col}'
    with open('allotted_seats.json', 'w') as f:
        json.dump(seats, f)

def get_seat(email: str) -> str:
    if needs_random('seating_timestamp'):
        reallocate()
        write_time('seating_timestamp')
    with open('allotted_seats.json', 'r') as f:
        seats = json.load(f)
    return seats.get(email, "Your email is not registered. Please contact the nearest mentor")

