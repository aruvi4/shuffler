import time

ONE_DAY = 24 * 60 * 60

def read_time(filename: str) -> int:
    with open(filename, 'r') as f:
        return float(f.read().strip())

def write_time(filename: str) -> None:
    with open(filename, 'w') as f:
        f.write(str(time.time()))

def needs_random(filename: str) -> bool:
    return time.time() - read_time(filename) > ONE_DAY