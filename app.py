from flask import Flask
import evaluations, seating


app = Flask(__name__)

@app.route("/seating/<email>")
def get_seat(email: str) -> str:
    return seating.get_seat(email)

@app.route("/evaluatees/<evaluator_email>")
def get_evaluatees(evaluator_email: str) -> str:
    return evaluations.get_evaluatees(evaluator_email)